import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:masjidnear_me/config/urls.dart';

class ImageView extends StatelessWidget {
  const ImageView({
    super.key,
    required this.imageUrl,
    this.width,
    this.height,
    this.fit,
  });

  final String? imageUrl;
  final double? width, height;
  final BoxFit? fit;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      clipBehavior: Clip.hardEdge,
      child: imageUrl?.trim() == baseUrl || (imageUrl?.trim().isEmpty ?? false) || imageUrl == null
          ? SizedBox(
              width: width,
              height: height,
              child: Image.asset(
                "assets/images/masjid_placeholder.jpg",
                fit: fit,
              ))
          : CachedNetworkImage(
              imageUrl: imageUrl ?? "",
              width: width,
              height: height,
              fit: fit,
              errorWidget: (context, url, error) {
                return SizedBox(width: width, height: height, child: Image.asset("assets/images/masjid_placeholder.jpg"));
              },
              placeholder: (context, url) {
                return SizedBox(width: width, height: height, child: Image.asset("assets/images/masjid_placeholder.jpg"));
              },
            ),
    );
  }
}
