import 'dart:async';

import 'package:flutter/material.dart';
import 'package:masjidnear_me/config/colors.dart';

class CustomInputField extends StatefulWidget {
  const CustomInputField({
    super.key,
    this.isRequired = false,
    this.hint,
    this.initialValue,
    this.onSave,
    this.prefix,
    this.suffix,
    this.onChange,
    this.onSubmit,
    this.inputType,
    this.showLabel = false,
    this.filled = true,
    this.border,
    this.controller,
    this.textAlign = TextAlign.start,
  });
  final TextEditingController? controller;
  final bool isRequired;
  final String? initialValue;
  final Function(String? value)? onSave;
  final Function(String? value)? onChange;
  final Function(String? value)? onSubmit;
  final Widget? prefix, suffix;
  final String? hint;
  final TextInputType? inputType;
  final bool showLabel;
  final InputBorder? border;
  final bool filled;
  final TextAlign textAlign;

  @override
  State<CustomInputField> createState() => _CustomInputFieldState();
}

class _CustomInputFieldState extends State<CustomInputField> {
  late final TextEditingController controller;
  Timer? _debounce;

  @override
  void initState() {
    controller = widget.controller ?? TextEditingController();

    if (widget.initialValue != null) {
      controller.text = widget.initialValue!;
    }
    super.initState();
  }

  @override
  void dispose() {
    // controller.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  void _onSearchChanged(String query) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      widget.onChange?.call(query);
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: widget.inputType,
      textAlign: widget.textAlign,
      decoration: InputDecoration(
        fillColor: Colors.white,
        hintText: widget.hint,
        hintStyle: const TextStyle(
          fontSize: 14,
        ),
        border: widget.border ?? OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(color: MyColors.primary)),
        label: widget.showLabel && widget.hint != null
            ? Text(
                widget.hint!,
              )
            : null,
        focusColor: Colors.grey,
        prefixIcon: widget.prefix,
        suffixIcon: widget.suffix,
        prefixIconColor: Colors.grey,
        contentPadding: const EdgeInsets.all(14),
        filled: widget.filled,
      ),
      onChanged: _onSearchChanged,
      onFieldSubmitted: (value) {
        widget.onSubmit?.call(value);
      },
      validator: (value) {
        if ((value == null || value.trim().isEmpty) && widget.isRequired) return "Field Required";
        return null;
      },
      onSaved: widget.onSave,
    );
  }
}
