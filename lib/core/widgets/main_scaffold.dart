// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:masjidnear_me/config/colors.dart';
import 'package:masjidnear_me/config/routing/app_router.dart';
import 'package:masjidnear_me/config/string_manager.dart';

class MainScaffold extends StatelessWidget {
  const MainScaffold({
    Key? key,
    required this.child,
    this.showAppBar = true,
    this.fab,
  }) : super(key: key);

  final Widget child;
  final bool showAppBar;
  final Widget? fab;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: showAppBar ? const CustomAppBar() : null,
      floatingActionButton: fab,
      body: child,
    );
  }
}

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    StringManager.salam.tr(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: MyColors.primary,
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(height: 4),
                  Text(
                    DateFormat("EEEEEEE, d MMMM yyyy").format(DateTime.now()),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: () {
                context.pushRoute(const MapRoute());
              },
              child: const Icon(
                Icons.map,
                color: Colors.orange,
                size: 32,
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(90);
}
