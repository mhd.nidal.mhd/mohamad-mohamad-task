class Failure {
  String? message;
  String? statusCode;

  Failure({
    this.message,
    this.statusCode,
  });
}
