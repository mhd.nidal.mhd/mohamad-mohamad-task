class ErrorModel {
    String? message;
    String? exceptionMessage;
    String? exceptionType;
    String? stackTrace;

    ErrorModel({
        this.message,
        this.exceptionMessage,
        this.exceptionType,
        this.stackTrace,
    });

    factory ErrorModel.fromJson(Map<String, dynamic> json) => ErrorModel(
        message: json["Message"],
        exceptionMessage: json["ExceptionMessage"],
        exceptionType: json["ExceptionType"],
        stackTrace: json["StackTrace"],
    );

    Map<String, dynamic> toJson() => {
        "Message": message,
        "ExceptionMessage": exceptionMessage,
        "ExceptionType": exceptionType,
        "StackTrace": stackTrace,
    };
}
