// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i8;

import '../config/register_module.dart' as _i10;
import '../config/routing/app_router.dart' as _i3;
import '../features/masjids/data/data_sources/main_ds.dart' as _i5;
import '../features/masjids/data/repositories/main_repo_impl.dart' as _i7;
import '../features/masjids/domain/repositories/main_repo.dart' as _i6;
import '../features/masjids/presentation/cubit/main_cubit.dart' as _i9;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final registerModule = _$RegisterModule();
    gh.singleton<_i3.AppRouter>(registerModule.appRouter);
    gh.singleton<_i4.Dio>(registerModule.getDio());
    gh.lazySingleton<_i5.MainDS>(() => _i5.MainDSImpl(gh<_i4.Dio>()));
    gh.lazySingleton<_i6.MainRepo>(() => _i7.MainRepoImpl(gh<_i5.MainDS>()));
    await gh.factoryAsync<_i8.SharedPreferences>(
      () => registerModule.sharedPreferences,
      preResolve: true,
    );
    gh.factory<_i9.MainCubit>(() => _i9.MainCubit(gh<_i6.MainRepo>()));
    return this;
  }
}

class _$RegisterModule extends _i10.RegisterModule {}
