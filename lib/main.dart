import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:in_app_notification/in_app_notification.dart';
import 'package:masjidnear_me/features/masjids/presentation/cubit/main_cubit.dart';
import 'config/colors.dart';
import 'config/routing/app_router.dart';
import 'di/injectable.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  configureDependencies();
  runApp(
    EasyLocalization(
      supportedLocales: const [
        Locale('en', 'US'),
        Locale('ar', 'SY'),
      ],
      path: 'assets/translations',
      fallbackLocale: const Locale('en', 'US'),
      useOnlyLangCode: true,
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(  
      create: (context) => getIt<MainCubit>(),
      child: ResponsiveSizer(
        builder: (p0, p1, p2) {
          return InAppNotification(
            child: MaterialApp.router(
              title: 'MasjidNearMe',
              localizationsDelegates: context.localizationDelegates,
              supportedLocales: context.supportedLocales,
              locale: context.locale,
              routerConfig: getIt<AppRouter>().config(),
              theme: ThemeData(
                colorScheme: ColorScheme.fromSeed(seedColor: MyColors.primary),
                useMaterial3: false,
              ),
            ),
          );
        },
      ),
    );
  }
}
