import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'routing/app_router.dart';
import 'funcs.dart';

@module
abstract class RegisterModule {
  @preResolve
  Future<SharedPreferences> get sharedPreferences => SharedPreferences.getInstance();

  @singleton
  AppRouter get appRouter => AppRouter();

  @singleton
  Dio getDio() {
    Dio dio = Dio()..options.connectTimeout = const Duration(seconds: 25);

    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) {
        handler.next(options);
      },
      onError: (e, handler) {
        eplog(e);
        handler.next(e);
      },
      onResponse: (e, handler) {
        handler.next(e);
      },
    ));

    return dio;
  }
}
