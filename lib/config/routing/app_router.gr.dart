// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    HomeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const HomePage(),
      );
    },
    MapRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MapPage(),
      );
    },
    MasjidDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<MasjidDetailsRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: MasjidDetailsPage(
          key: args.key,
          masjid: args.masjid,
        ),
      );
    },
    SetPointOnMapRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SetPointOnMapPage(),
      );
    },
  };
}

/// generated route for
/// [HomePage]
class HomeRoute extends PageRouteInfo<void> {
  const HomeRoute({List<PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [MapPage]
class MapRoute extends PageRouteInfo<void> {
  const MapRoute({List<PageRouteInfo>? children})
      : super(
          MapRoute.name,
          initialChildren: children,
        );

  static const String name = 'MapRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [MasjidDetailsPage]
class MasjidDetailsRoute extends PageRouteInfo<MasjidDetailsRouteArgs> {
  MasjidDetailsRoute({
    Key? key,
    required MasjidModel masjid,
    List<PageRouteInfo>? children,
  }) : super(
          MasjidDetailsRoute.name,
          args: MasjidDetailsRouteArgs(
            key: key,
            masjid: masjid,
          ),
          initialChildren: children,
        );

  static const String name = 'MasjidDetailsRoute';

  static const PageInfo<MasjidDetailsRouteArgs> page =
      PageInfo<MasjidDetailsRouteArgs>(name);
}

class MasjidDetailsRouteArgs {
  const MasjidDetailsRouteArgs({
    this.key,
    required this.masjid,
  });

  final Key? key;

  final MasjidModel masjid;

  @override
  String toString() {
    return 'MasjidDetailsRouteArgs{key: $key, masjid: $masjid}';
  }
}

/// generated route for
/// [SetPointOnMapPage]
class SetPointOnMapRoute extends PageRouteInfo<void> {
  const SetPointOnMapRoute({List<PageRouteInfo>? children})
      : super(
          SetPointOnMapRoute.name,
          initialChildren: children,
        );

  static const String name = 'SetPointOnMapRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
