import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:masjidnear_me/features/masjids/domain/models/masjid_model.dart';
import 'package:masjidnear_me/features/masjids/presentation/pages/home_page.dart';
import 'package:masjidnear_me/features/masjids/presentation/pages/map_page.dart';
import 'package:masjidnear_me/features/masjids/presentation/pages/masjid_details_page.dart';
import 'package:masjidnear_me/features/masjids/presentation/pages/set_point_on_map.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        CustomRoute(transitionsBuilder: TransitionsBuilders.fadeIn, page: HomeRoute.page, initial: true),
        CustomRoute(transitionsBuilder: TransitionsBuilders.fadeIn, page: MapRoute.page),
        CustomRoute(transitionsBuilder: TransitionsBuilders.fadeIn, page: MasjidDetailsRoute.page),
        CustomRoute(transitionsBuilder: TransitionsBuilders.fadeIn, page: SetPointOnMapRoute.page),
      ];
}
