import 'dart:developer';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'funcs.dart';
import '../core/models/failures.dart';

Future<Either<Failure, T>> sendRequest<T>(
  Function call, {
  Function(T value)? cacheCall,
}) async {
  try {
    final result = await call();

    if (cacheCall != null) cacheCall(result);
    return Right(result);
  } on DioException catch (e) {
    eplog(e.response);
    eplog(e);
    inspect(e);
    //TODO : handle the error response according to the api

    try {
      return Left(Failure(
        statusCode: (e.response?.statusCode ?? -1).toString(),
        message: e.response?.data['exception'] ?? e.response?.statusMessage ?? e.message ?? e.toString(),
      ));
    } catch (e) {
      return Left(Failure(message: "Error in parsing error"));
    }
  } on HttpException catch (e) {
    return Left(Failure(
      statusCode: "Http Exception",
      message: e.message,
    ));
  } on Exception catch (e) {
    return Left(Failure(
      statusCode: "Exception",
      message: e.toString(),
    ));
  }
}
