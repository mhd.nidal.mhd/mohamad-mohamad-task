import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:in_app_notification/in_app_notification.dart';
import 'package:logger/logger.dart';
import 'package:map_launcher/map_launcher.dart';

void plog(dynamic message) {
  Logger().d(message);
}

void eplog(dynamic message) {
  Logger().e(message);
}

List<T> parseListResponse<T>(Response response, T Function(Map<String, dynamic> json) fromJson) {
  final data = response.data as List;
  return data.map((e) => fromJson(e)).toList();
}

MaterialColor getMaterialColor(Color color) {
  final int red = color.red;
  final int green = color.green;
  final int blue = color.blue;

  final Map<int, Color> shades = {
    50: Color.fromRGBO(red, green, blue, .1),
    100: Color.fromRGBO(red, green, blue, .2),
    200: Color.fromRGBO(red, green, blue, .3),
    300: Color.fromRGBO(red, green, blue, .4),
    400: Color.fromRGBO(red, green, blue, .5),
    500: Color.fromRGBO(red, green, blue, .6),
    600: Color.fromRGBO(red, green, blue, .7),
    700: Color.fromRGBO(red, green, blue, .8),
    800: Color.fromRGBO(red, green, blue, .9),
    900: Color.fromRGBO(red, green, blue, 1),
  };

  return MaterialColor(color.value, shades);
}

Future<void> launchMapsUrl(double lat, double lon, String name) async {
  final availableMaps = await MapLauncher.installedMaps;
  await availableMaps.first.showMarker(coords: Coords(lat, lon), title: name);
}



Future<void> showErrorNotification(BuildContext context, String message) async {
  await showToast(
    context,
    message,
    Colors.red,
    Icons.error,
  );
}

Future<void> showSucessNotification(BuildContext context, String message) async {
  await showToast(
    context,
    message,
    Colors.green,
    Icons.check,
  );
}

Future<void> showToast(BuildContext context, String message, Color color, IconData icon) async {
  await InAppNotification.show(
    duration: const Duration(seconds: 1),
    child: Container(
      margin: const EdgeInsets.all(24),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: color,
      ),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              icon,
              color: Colors.white,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                message,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    ),
    context: context,
  );
}