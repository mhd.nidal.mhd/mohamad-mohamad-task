// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:masjidnear_me/config/funcs.dart';

class LocationManager {
  static LatLng currentLocation = const LatLng(12.3682265169693, 76.6509528432083);
  static bool gotLocation = false;

  static Future<bool> handleLocationPermission(BuildContext context) async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      showErrorNotification(context, "Location services are disabled. Please enable the services");
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        showErrorNotification(context, "Location permissions are denied");
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      showErrorNotification(context, "Location permissions are permanently denied, we cannot request permissions.");
      return false;
    }
    return true;
  }

  static Future<LatLng> getUserLocation() async {
    if (gotLocation) return currentLocation;
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    currentLocation = LatLng(position.latitude, position.longitude);
    gotLocation = true;
    return currentLocation;
  }
}
