import 'package:flutter/material.dart';
import 'package:masjidnear_me/config/funcs.dart';

class MyColors {
  static MaterialColor primary = getMaterialColor(const Color(0xFF025E03));
}
