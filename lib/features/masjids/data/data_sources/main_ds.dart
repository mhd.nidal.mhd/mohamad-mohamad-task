import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import '../../../../config/funcs.dart';
import '../../../../config/urls.dart';
import '../../domain/models/masjid_model.dart';
import '../../domain/params/main_params.dart';

abstract class MainDS {
  Future<List<MasjidModel>> getMasjids(MainParams params);
}

@LazySingleton(as: MainDS)
class MainDSImpl extends MainDS {
  final Dio dio;
  MainDSImpl(this.dio);

  @override
  Future<List<MasjidModel>> getMasjids(MainParams params) async {
    final result = await dio.get("$baseUrl${params.toQuery()}");
    if (result.statusCode == 204) {
      return [];
    } else {
      return parseListResponse(result, (json) => MasjidModel.fromJson(json));
    }
  }
}
