import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:masjidnear_me/features/masjids/data/data_sources/main_ds.dart';
import '../../../../config/response_handler.dart';
import '../../../../core/models/failures.dart';
import '../../domain/models/masjid_model.dart';
import '../../domain/params/main_params.dart';
import '../../domain/repositories/main_repo.dart';

@LazySingleton(as: MainRepo)
class MainRepoImpl extends MainRepo {
  final MainDS _remoteDS;
  MainRepoImpl(this._remoteDS);

  @override
  Future<Either<Failure, List<MasjidModel>>> getMasjids(MainParams params) {
    return sendRequest(() => _remoteDS.getMasjids(params));
  }
}
