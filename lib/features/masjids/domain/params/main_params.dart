// ignore_for_file: public_member_api_docs, sort_constructors_first
class MainParams {
  double? lat;
  double? long;
  int? radius;

  MainParams({
    this.lat,
    this.long,
    this.radius = 200 * 1000, // 200 km
  });

  String toQuery() => "query/lat\$$lat,lng\$$long,rad\$$radius/";
}
