import 'package:google_maps_flutter/google_maps_flutter.dart';

class MasjidLocation {
  String? type;
  List<double>? coordinates;

  MasjidLocation({
    this.type,
    this.coordinates,
  });

  factory MasjidLocation.fromJson(Map<String, dynamic> json) => MasjidLocation(
        type: json["type"],
        coordinates: json["coordinates"] == null ? [] : List<double>.from(json["coordinates"]!.map((x) => x?.toDouble())),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "coordinates": coordinates == null ? [] : List<dynamic>.from(coordinates!.map((x) => x)),
      };

  LatLng get latlng => LatLng(coordinates![1], coordinates![0]);
}
