
import 'masjid_address_model.dart';
import 'masjid_location_model.dart';
import 'masjid_timing_model.dart';

class MasjidModel {
    String? id;
    String? masjidId;
    String? masjidName;
    MasjidAddress? masjidAddress;
    MasjidLocation? masjidLocation;
    MasjidTimings? masjidTimings;
    String? masjidCreatedby;
    String? masjidModifiedby;
    DateTime? masjidCreatedon;
    DateTime? masjidModifiedon;
    dynamic masjidPic;
    dynamic distance;
    bool? notMasjid;

    MasjidModel({
        this.id,
        this.masjidId,
        this.masjidName,
        this.masjidAddress,
        this.masjidLocation,
        this.masjidTimings,
        this.masjidCreatedby,
        this.masjidModifiedby,
        this.masjidCreatedon,
        this.masjidModifiedon,
        this.masjidPic,
        this.distance,
        this.notMasjid,
    });

    factory MasjidModel.fromJson(Map<String, dynamic> json) => MasjidModel(
        id: json["_id"],
        masjidId: json["masjidId"],
        masjidName: json["masjidName"],
        masjidAddress: json["masjidAddress"] == null ? null : MasjidAddress.fromJson(json["masjidAddress"]),
        masjidLocation: json["masjidLocation"] == null ? null : MasjidLocation.fromJson(json["masjidLocation"]),
        masjidTimings: json["masjidTimings"] == null ? null : MasjidTimings.fromJson(json["masjidTimings"]),
        masjidCreatedby: json["masjidCreatedby"],
        masjidModifiedby: json["masjidModifiedby"],
        masjidCreatedon: json["masjidCreatedon"] == null ? null : DateTime.parse(json["masjidCreatedon"]),
        masjidModifiedon: json["masjidModifiedon"] == null ? null : DateTime.parse(json["masjidModifiedon"]),
        masjidPic: json["masjidPic"],
        distance: json["Distance"],
        notMasjid: json["notMasjid"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "masjidId": masjidId,
        "masjidName": masjidName,
        "masjidAddress": masjidAddress?.toJson(),
        "masjidLocation": masjidLocation?.toJson(),
        "masjidTimings": masjidTimings?.toJson(),
        "masjidCreatedby": masjidCreatedby,
        "masjidModifiedby": masjidModifiedby,
        "masjidCreatedon": masjidCreatedon?.toIso8601String(),
        "masjidModifiedon": masjidModifiedon?.toIso8601String(),
        "masjidPic": masjidPic,
        "Distance": distance,
        "notMasjid": notMasjid,
    };
}




