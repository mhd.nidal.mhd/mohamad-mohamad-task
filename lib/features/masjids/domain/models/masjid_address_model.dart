class MasjidAddress {
  String? description;
  String? street;
  String? zipcode;
  String? country;
  String? state;
  String? city;
  String? locality;
  String? phone;
  String? googlePlaceId;

  MasjidAddress({
    this.description,
    this.street,
    this.zipcode,
    this.country,
    this.state,
    this.city,
    this.locality,
    this.phone,
    this.googlePlaceId,
  });

  factory MasjidAddress.fromJson(Map<String, dynamic> json) => MasjidAddress(
        description: json["description"],
        street: json["street"],
        zipcode: json["zipcode"],
        country: json["country"],
        state: json["state"],
        city: json["city"],
        locality: json["locality"],
        phone: json["phone"] != null && json["phone"] != "" ? json["phone"] : " - ",
        googlePlaceId: json["googlePlaceId"],
      );

  Map<String, dynamic> toJson() => {
        "description": description,
        "street": street,
        "zipcode": zipcode,
        "country": country,
        "state": state,
        "city": city,
        "locality": locality,
        "phone": phone,
        "googlePlaceId": googlePlaceId,
      };

  String get fullAddress => "$country - $state - $city";
}
