class MasjidTimings {
  String? fajr;
  String? zuhr;
  String? asr;
  String? maghrib;
  String? isha;
  String? jumah;

  MasjidTimings({
    this.fajr,
    this.zuhr,
    this.asr,
    this.maghrib,
    this.isha,
    this.jumah,
  });

  factory MasjidTimings.fromJson(Map<String, dynamic> json) => MasjidTimings(
        fajr: json["fajr"] != null && json["fajr"] != "" ? json["fajr"] : " - ",
        zuhr: json["zuhr"] != null && json["zuhr"] != "" ? json["zuhr"] : " - ",
        asr: json["asr"] != null && json["asr"] != "" ? json["asr"] : " - ",
        maghrib: json["maghrib"] != null && json["maghrib"] != "" ? json["maghrib"] : " - ",
        isha: json["isha"] != null && json["isha"] != "" ? json["isha"] : " - ",
        jumah: json["jumah"] != null && json["jumah"] != "" ? json["jumah"] : " - ",
      );

  Map<String, dynamic> toJson() => {
        "fajr": fajr,
        "zuhr": zuhr,
        "asr": asr,
        "maghrib": maghrib,
        "isha": isha,
        "jumah": jumah,
      };

  List<(String, String)> get prayers => [
        ("fajr", fajr ?? " - "),
        ("zuhr", zuhr ?? " - "),
        ("asr", asr ?? " - "),
        ("maghrib", maghrib ?? " - "),
        ("isha", isha ?? " - "),
      ];
}
