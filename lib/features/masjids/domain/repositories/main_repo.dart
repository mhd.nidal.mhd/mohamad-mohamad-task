import 'package:dartz/dartz.dart';
import '../../../../core/models/failures.dart';
import '../models/masjid_model.dart';
import '../params/main_params.dart';

abstract class MainRepo {
  Future<Either<Failure, List<MasjidModel>>> getMasjids(MainParams params);
}
