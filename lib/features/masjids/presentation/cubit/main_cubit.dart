import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:masjidnear_me/features/masjids/domain/params/main_params.dart';
import 'package:masjidnear_me/features/masjids/domain/repositories/main_repo.dart';

import '../../domain/models/masjid_model.dart';

part 'main_state.dart';
part 'main_cubit.freezed.dart';

@injectable
class MainCubit extends Cubit<MainState> {
  final MainRepo _repo;
  MainCubit(this._repo) : super(const MainState.initial());

  Future<void> getMasjids(MainParams params) async {
    emit(const MainState.loading());
    final result = await _repo.getMasjids(params);
    result.fold(
      (l) => emit(MainState.error(l.message!)),
      (r) => emit(MainState.loaded(r)),
    );
  }
}
