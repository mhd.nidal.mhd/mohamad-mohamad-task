import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:masjidnear_me/config/colors.dart';
import 'package:masjidnear_me/config/routing/app_router.dart';
import 'package:masjidnear_me/core/widgets/image_view.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../domain/models/masjid_model.dart';

class MasjidItemWidget extends StatelessWidget {
  const MasjidItemWidget({
    super.key,
    required this.masjid,
  });
  final MasjidModel masjid;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        context.pushRoute(MasjidDetailsRoute(masjid: masjid));
      },
      child: Container(
        width: 100.w,
        height: 210,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: MyColors.primary.withAlpha(20),
              spreadRadius: 4,
              blurRadius: 4,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: ImageView(
                imageUrl: masjid.masjidPic,
                fit: BoxFit.fitWidth,
                width: 100.w,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      masjid.masjidName ?? "",
                      style: const TextStyle(
                        height: 1,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.pin_drop_rounded,
                          color: MyColors.primary,
                          size: 14,
                        ),
                        Expanded(
                          child: Text(
                            masjid.masjidAddress?.fullAddress ?? "",
                            style: const TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
