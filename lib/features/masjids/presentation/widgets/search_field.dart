import 'package:flutter/material.dart';
import 'package:masjidnear_me/core/widgets/custom_input_field.dart';

class SearchField extends StatelessWidget {
  const SearchField({
    super.key,
    this.onChanged,
  });
  final Function(String? string)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: CustomInputField(
        hint: "Search ...",
        prefix: const Padding(
          padding: EdgeInsets.all(8.0),
          child: Icon(
            Icons.search,
          ),
        ),
        onChange: (value) {
          onChanged?.call(value);
        },
        onSave: (_) {},
      ),
    );
  }
}
