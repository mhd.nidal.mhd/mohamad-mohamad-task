import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:masjidnear_me/config/colors.dart';
import 'package:masjidnear_me/config/location_manager.dart';
import 'package:masjidnear_me/config/string_manager.dart';
import 'package:masjidnear_me/core/widgets/main_scaffold.dart';

@RoutePage()
class SetPointOnMapPage extends StatefulWidget {
  const SetPointOnMapPage({super.key});

  @override
  State<SetPointOnMapPage> createState() => _SetPointOnMapPageState();
}

class _SetPointOnMapPageState extends State<SetPointOnMapPage> {
  late final CameraPosition _kGooglePlex = CameraPosition(
    target: LocationManager.currentLocation,
    zoom: 14.4746,
  );
  final Completer<GoogleMapController> _controller = Completer<GoogleMapController>();
  LatLng? centerPos;

  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      showAppBar: false,
      child: Stack(
        children: [
          GoogleMap(
            initialCameraPosition: _kGooglePlex,
            mapType: MapType.normal,
            padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 8),
            onCameraMove: (position) {
              centerPos = position.target;
              setState(() {});
            },
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
          ),
          Align(
            alignment: Alignment.center,
            child: Icon(
              Icons.person_pin_circle_rounded,
              color: MyColors.primary,
              size: 34,
            ),
          ),
          if (centerPos != null)
            Align(
              alignment: AlignmentDirectional.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(kBottomNavigationBarHeight),
                child: ElevatedButton(
                  onPressed: () {
                    LocationManager.currentLocation = centerPos!;
                    context.popRoute();
                  },
                  child: Text(
                    StringManager.selectLocation.tr(),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
