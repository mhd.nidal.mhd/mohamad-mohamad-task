import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lottie/lottie.dart';
import 'package:masjidnear_me/config/location_manager.dart';
import 'package:masjidnear_me/config/routing/app_router.dart';
import 'package:masjidnear_me/core/widgets/main_scaffold.dart';
import 'package:masjidnear_me/features/masjids/domain/params/main_params.dart';
import 'package:masjidnear_me/features/masjids/presentation/cubit/main_cubit.dart';
import 'package:masjidnear_me/features/masjids/presentation/widgets/masjid_item_widget.dart';
import 'package:masjidnear_me/features/masjids/presentation/widgets/search_field.dart';

@RoutePage()
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final MainCubit cubit;
  String query = "";

  @override
  void initState() {
    cubit = context.read<MainCubit>();

    super.initState();
  }

  Future<LatLng> getLocation() async {
    await LocationManager.handleLocationPermission(context);
    var latlng = await LocationManager.getUserLocation();

    getData(latlng);

    return latlng;
  }

  void getData(LatLng latlng) {
    cubit.getMasjids(MainParams(
      lat: latlng.latitude,
      long: latlng.longitude,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      fab: FloatingActionButton.extended(
        onPressed: () async {
          await context.pushRoute(const SetPointOnMapRoute());
          getData(LocationManager.currentLocation);
        },
        label: const Text("Change Location"),
      ),
      child: FutureBuilder(
        future: getLocation(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return const Center(child: CircularProgressIndicator());
          return Column(
            children: [
              SearchField(
                onChanged: (string) {
                  setState(() {
                    query = string ?? "";
                  });
                },
              ),
              Expanded(
                child: BlocBuilder<MainCubit, MainState>(
                  builder: (context, state) {
                    return state.maybeWhen(
                      orElse: () => const Center(
                        child: CircularProgressIndicator(),
                      ),
                      loaded: (masjids) {
                        var filteredMasjids = masjids.where((element) => (element.masjidName ?? "").toLowerCase().startsWith(query.toLowerCase())).toList();
                        if (filteredMasjids.isEmpty) {
                          return Column(
                            children: [
                              Lottie.asset("assets/images/empty_data.json"),
                              const SizedBox(
                                height: 16,
                              ),
                              Text("Found ${filteredMasjids.length} Masjids Near You"),
                            ],
                          );
                        }
                        return Column(
                          children: [
                            const SizedBox(height: 16),
                            Text("Found ${filteredMasjids.length} Masjids Near You"),
                            const SizedBox(height: 16),
                            Expanded(
                              child: ListView.separated(
                                padding: const EdgeInsets.fromLTRB(24, 16, 24, kBottomNavigationBarHeight),
                                separatorBuilder: (context, index) {
                                  return const SizedBox(height: 16);
                                },
                                itemBuilder: (context, index) {
                                  return MasjidItemWidget(masjid: filteredMasjids[index]);
                                },
                                itemCount: filteredMasjids.length,
                              ),
                            ),
                          ],
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
