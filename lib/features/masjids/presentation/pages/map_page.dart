import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:clippy_flutter/triangle.dart';
import 'package:custom_info_window/custom_info_window.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:masjidnear_me/config/funcs.dart';
import 'package:masjidnear_me/config/location_manager.dart';
import 'package:masjidnear_me/features/masjids/domain/models/masjid_model.dart';
import 'package:masjidnear_me/features/masjids/domain/params/main_params.dart';
import 'package:masjidnear_me/features/masjids/presentation/cubit/main_cubit.dart';

@RoutePage()
class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  late final CameraPosition _kGooglePlex = CameraPosition(
    target: LocationManager.currentLocation,
    zoom: 14.4746,
  );

  late LatLng pos = LocationManager.currentLocation;

  late final MainCubit cubit;

  Set<Marker> markers = {};

  @override
  void initState() {
    cubit = context.read<MainCubit>();
    super.initState();
  }

  void _getData(LatLng pos) {
    cubit.getMasjids(MainParams(
      lat: pos.latitude,
      long: pos.longitude,
    ));
  }

  BitmapDescriptor? markerIcon;

  Future<BitmapDescriptor> _loadMarkerImage() async {
    markerIcon = await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(
        size: Size(10, 10),
      ),
      "assets/images/marker-3.png",
    );
    _getData(pos);
    setState(() {});
    return markerIcon!;
  }

  LatLng? centerPos;

  final CustomInfoWindowController _customInfoWindowController = CustomInfoWindowController();

  void onTap(MasjidModel masjid) {
    _customInfoWindowController.addInfoWindow!(
      Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4),
              ),
              width: double.infinity,
              height: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(child: Text(masjid.masjidName ?? "- ")),
                    const SizedBox(
                      width: 16,
                    ),
                    InkWell(
                        onTap: () {
                          launchMapsUrl(masjid.masjidLocation!.latlng.latitude, masjid.masjidLocation!.latlng.longitude, masjid.masjidName!);
                        },
                        child: const Icon(
                          Icons.map,
                          color: Colors.orange,
                        )),
                  ],
                ),
              ),
            ),
          ),
          Triangle.isosceles(
            edge: Edge.BOTTOM,
            child: Container(
              color: Colors.white,
              width: 20.0,
              height: 10.0,
            ),
          ),
        ],
      ),
      masjid.masjidLocation!.latlng,
    );
  }

  @override
  Widget build(BuildContext context) {
    var googleMap = GoogleMap(
      mapType: MapType.normal,
      padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 8),
      initialCameraPosition: _kGooglePlex,
      onTap: (position) {
        _customInfoWindowController.hideInfoWindow?.call();
      },
      onCameraMove: (position) {
        _customInfoWindowController.onCameraMove?.call();
        centerPos = position.target;
        setState(() {});
      },
      onCameraIdle: () {
        _getData(centerPos ?? pos);
      },
      onMapCreated: (GoogleMapController controller) async {
        _customInfoWindowController.googleMapController = controller;
      },
      markers: markers,
    );
    return Scaffold(
      body: BlocListener<MainCubit, MainState>(
        listener: (context, state) {
          state.maybeWhen(
            orElse: () {},
            loaded: (masjids) {
              setState(() {
                markers = masjids
                    .map(
                      (e) => Marker(
                        markerId: MarkerId(e.id!),
                        position: e.masjidLocation!.latlng,
                        icon: markerIcon!,
                        onTap: () {
                          onTap(e);
                        },
                      ),
                    )
                    .toSet();
              });
            },
          );
        },
        child: Stack(
          children: [
            if (markerIcon == null)
              FutureBuilder(
                future: _loadMarkerImage(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return googleMap;
                },
              )
            else
              googleMap,
            CustomInfoWindow(
              controller: _customInfoWindowController,
              height: 75,
              width: 200,
              offset: 50,
            ),
            const BackBtn(),
          ],
        ),
      ),
    );
  }
}

class BackBtn extends StatelessWidget {
  const BackBtn({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: InkWell(
        onTap: () {
          context.popRoute();
        },
        child: const Padding(
          padding: EdgeInsets.all(16),
          child: Icon(
            Icons.arrow_back_ios_new,
            color: Colors.orange,
            size: 32,
          ),
        ),
      ),
    );
  }
}
