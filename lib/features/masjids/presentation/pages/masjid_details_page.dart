import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:masjidnear_me/config/colors.dart';
import 'package:masjidnear_me/config/funcs.dart';
import 'package:masjidnear_me/config/string_manager.dart';
import 'package:masjidnear_me/core/widgets/image_view.dart';
import 'package:masjidnear_me/features/masjids/domain/models/masjid_model.dart';
import 'package:masjidnear_me/features/masjids/presentation/pages/map_page.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

@RoutePage()
class MasjidDetailsPage extends StatelessWidget {
  const MasjidDetailsPage({super.key, required this.masjid});

  final MasjidModel masjid;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ImageView(
            imageUrl: masjid.masjidPic,
          ),
          Container(
            width: 100.w,
            height: 100.h,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.transparent,
                  Colors.white,
                ],
                begin: AlignmentDirectional.topCenter,
                end: AlignmentDirectional.bottomCenter,
                stops: [
                  0,
                  0.35,
                ],
              ),
            ),
          ),
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  width: 100.w,
                  margin: EdgeInsets.fromLTRB(24, 26.h, 24, 0),
                  padding: const EdgeInsets.all(24),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16),
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.primary.withAlpha(20),
                        spreadRadius: 4,
                        blurRadius: 4,
                        offset: const Offset(0, 2),
                      ),
                    ],
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        masjid.masjidName ?? "",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: [
                          const Icon(
                            Icons.pin_drop_rounded,
                            color: Colors.orange,
                            size: 24,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Text(
                              masjid.masjidAddress?.fullAddress ?? "",
                              style: const TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: [
                          const Icon(
                            Icons.phone,
                            color: Colors.orange,
                            size: 24,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: Text(
                              masjid.masjidAddress?.phone ?? " - ",
                              style: const TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {
                              launchMapsUrl(masjid.masjidLocation!.latlng.latitude, masjid.masjidLocation!.latlng.longitude, masjid.masjidName!);
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                  color: MyColors.primary,
                                ),
                              ),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.map,
                                    color: MyColors.primary,
                                    size: 22,
                                  ),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  Text(StringManager.findOnMap.tr())
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Text(
                          StringManager.prayer.tr(),
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          StringManager.adhan.tr(),
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ...(masjid.masjidTimings?.prayers ?? []).map(
                  (e) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 4,
                          child: Text(
                            e.$1.tr(),
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                            e.$2,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const BackBtn(),
        ],
      ),
    );
  }
}
